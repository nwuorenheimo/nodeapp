var os = require('os');

var message = 'Here is some information about your system';

var sysarray = new Array('Type: '+os.type(),
						'Node Version: '+process.version,
						/* process is global to node and isn't a module that needs to be specified */
						'Platform: '+os.platform(),
						'Hostname: '+os.hostname(),
						'Total memory: '+os.totalmem(),
						'Free memory: '+os.freemem(),
						'Uptime: '+os.uptime(),						
						);

console.log(message);
/* document.write()would be used for html, console.log() for the command line */

var arraylen = sysarray.length;

i = 0;
/* i is the counter variable */

while(i < arraylen){
console.log(sysarray[i]);
i++;

}
				